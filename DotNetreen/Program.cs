﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetreen
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: Dotnetreen <filename>");
            }

            // Start Coding!   
            Dictionary<int, string> dic = new Dictionary<int, string>();
            TextReader input = Console.In;
            if (args.Any())
            {
                var path = args[0];
                if (File.Exists(path))
                {
                    input = File.OpenText(path);
                    var alarms = File.ReadLines(path)
                               .Where(l => l.Contains('='))
                               .Select(line => line.Split(' '))
                               .ToArray();

                    var data = File.ReadLines(path)
                               .Where(l => l.Contains(':'))
                               .Select(line => line.Split(' '))
                               .ToArray();

                  

                    foreach(var item in data)
                    {
                            int key = -1;
                            int value = -1;
                            foreach (var i in item)
                            {
                                int index = i.IndexOf(':');
                                if (index > 0)
                                {
                                    key = Convert.ToInt32(i.Substring(0, index));
                                    value =Convert.ToInt32(i.Substring(index, (i.Length-index)).Replace(':',' '));
                                }
                                foreach(var al in alarms)
                                {
                                    foreach(var a in al)
                                    {
                                        int ONvalue = -100;
                                        int OFFvalue = 100;
                                        int ONindex = a.IndexOf("ON=");
                                        if (ONindex >= 0)
                                        {
                                            ONvalue = Convert.ToInt32(a.Replace("ON="," "));
                                        }
                                        int OFFindex = a.IndexOf("OFF=");
                                        if (OFFindex >= 0)
                                        {
                                            OFFvalue = Convert.ToInt32(a.Replace("OFF=", " "));
                                        }

                                        if(value == ONvalue)
                                        {
                                            string val ="["+key+"] " + al[0] + " ON WAS " + value;
                                            //Console.WriteLine(val);
                                            if(!dic.ContainsKey(key))
                                            dic.Add(key, val);
                                            
                                        }
                                        if (value == OFFvalue)
                                        {
                                            string val = "["+key+"] " + al[0] + " OFF WAS " + value;
                                            //Console.WriteLine(val);
                                            if (!dic.ContainsKey(key))
                                            dic.Add(key, val);
                                        }


                                    }
                                    
                                }
                            }
                    }


                }
            }
            var list = dic.Keys.ToList();
            list.Sort();
            foreach (var key in list)
            {
                Console.WriteLine("{0}",  dic[key]);
            }
            Console.ReadLine();

        }
    }
}
